<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'login_uuid', 'login_link', 'auth_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'login_uuid', 'login_link', 'auth_token',
    ];

    /**
     * @return bool
     */
    public function sendLoginLink()
    {
        try {
            $to_name = trim($this->name);
            $to_email = trim($this->email);
            $data = [
                'name' => env('MAIL_FROM_NAME'),
                'body' => "Please click below link to auto login",
            ];
            $this->login_uuid = null;
            $this->auth_token = null;
            $this->login_link = base64_encode($this->email . '_time_' . time());
            if ($this->save()) {
                $data['link'] = env('FRONTEND_APP_URL') . '/login?verifyLogin=' . urlencode($this->login_link);
                \Mail::send('emails.login_link', $data, function ($message) use ($to_name, $to_email) {
                    $message
                        ->to($to_email, $to_name)
                        ->subject('Login Link to Portal');
                });
                return true;
            }
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
        }
        return false;
    }

    /**
     * @param $email
     * @return |null
     */
    public static function findByEmail($email)
    {
        if(empty($email)) return null;

        return self::where('email', $email)->first();
    }

    /**
     * @param $email
     * @param $loginLink
     * @return |null
     */
    public static function findByEmailAndLoginLink($email, $loginLink)
    {
        if(empty($email) || empty($loginLink)) return null;

        return self::where([
            ['email', '=', $email],
            ['login_link', '=', $loginLink],
        ])->first();
    }

    /**
     * @param $linkUUID
     * @param $authToken
     * @return |null
     */
    public static function findByLoginUUIDAndAuthToken($linkUUID, $authToken)
    {
        if(empty($linkUUID) || empty($authToken)) return null;

        return self::where([
            ['login_uuid', '=', $linkUUID],
            ['auth_token', '=', $authToken],
        ])->first();
    }
}
