<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class CustomAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiKey = $request->header('X-Api-Key');
        if (empty($apiKey)) {
            return redirect('/');
        }
        $apiKeyArr = explode('_', $apiKey);
        $loginUUID = !empty($apiKeyArr[0]) ? $apiKeyArr[0] : null;
        $authToken = !empty($apiKeyArr[1]) ? $apiKeyArr[1] : null;

        $user = User::findByLoginUUIDAndAuthToken($loginUUID, $authToken);
        if (empty($user)) {
            return redirect('/');
        }
        \Auth::login($user);

        return $next($request);
    }
}
