<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('custom_auth');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(['status' => true, 'message' => 'Logged user', 'data' => \Auth::user()]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function userEmails()
    {
        try {
            $emails = "";
            if (\Storage::disk('local')->exists(\Auth::id() . '/emails.csv')) {
                $emails = \Storage::disk('local')->get(\Auth::id() . '/emails.csv');
            }
            return response()->json(['status' => true, 'message' => 'User emails', 'data' => $emails]);
        } catch (\Exception $exception) {
            return response()->json(['status' => true, 'message' => $exception->getMessage(), 'data' => null]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveUserEmails(Request $request)
    {
        try {
            $emails = "";
            if (!empty($request->emails)) {
                $emailsArr = array_map(function ($item) {
                    return trim($item, ' \'"');
                }, explode(',', $request->emails));

                $emailsArr = array_filter($emailsArr, function ($email) {
                    return filter_var($email, FILTER_VALIDATE_EMAIL);
                });

                $emails = rtrim(implode(',', $emailsArr), ',');
            }
            \Storage::disk('local')->put(\Auth::id() . '/emails.csv', $emails);
            return response()->json(['status' => true, 'message' => 'User emails saved', 'data' => $emails]);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage(), 'data' => null]);
        }
    }
}
