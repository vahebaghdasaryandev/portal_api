<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendLoginLink(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $credentials = $request->only('email');
        $user = User::findByEmail($credentials['email']);
        if (empty($user)) {
            return response()->json(['status' => false, 'message' => 'Invalid credentials', 'data' => null]);
        }

        if ($user->sendLoginLink()) {
            return response()->json(['status' => true, 'message' => 'The email sent. Please check your email.', 'data' => null]);
        }
        return response()->json(['status' => false, 'message' => 'Oops something went wrong. Please try again later', 'data' => null]);
    }

    /**
     * @param string $loginLink
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyLogin($loginLink)
    {
        if (empty($loginLink)) {
            return response()->json(['status' => false, 'message' => 'Invalid login link', 'data' => null]);
        }
        $decodedCodeArr = explode('_time_', base64_decode($loginLink));
        $email = !empty($decodedCodeArr[0]) ? $decodedCodeArr[0] : null;
        $sendTime = !empty($decodedCodeArr[1]) ? (int)$decodedCodeArr[1] : null;
        if (!empty($email) && !empty($sendTime)) {
            if ((time() - $sendTime) <= env('VERIFY_LOGIN_TIME', 600)) { // 10 min
                $user = User::findByEmailAndLoginLink($email, $loginLink);
                if (empty($user)) {
                    return response()->json(['status' => false, 'message' => 'Invalid login link', 'data' => null]);
                }
                $user->login_uuid = \Str::uuid()->toString();
                $user->auth_token = \Str::random(32);
                if ($user->save()) {
                    return response()->json([
                        'status' => true,
                        'message' => 'Success',
                        'data' => [
                            'login_uuid' => $user->login_uuid,
                            'auth_token' => $user->auth_token,
                            'user' => $user,
                        ]
                    ]);
                } else {
                    return response()->json(['status' => false, 'message' => 'Oops something went wrong. Please try again later', 'data' => null]);
                }
            } else {
                return response()->json(['status' => false, 'message' => 'The login link time expired', 'data' => null]);
            }
        }
        return response()->json(['status' => false, 'message' => 'Oops something went wrong. Please try again later', 'data' => null]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $user = \Auth::user();
        if (!empty($user)) {
            $user->login_uuid = null;
            $user->auth_token = null;
            $user->login_link = null;
            $user->save();
        }

        \Auth::logout();

        return response()->json(['status' => true, 'message' => 'Success', 'data' => null]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function accessDenied()
    {
        return response()->json(['status' => false, 'message' => 'Access denied', 'data' => null]);
    }
}
