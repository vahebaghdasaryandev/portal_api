## Setup
* Copy and change the environment variables from .env.example to .env
##### Important part of .env file 
```
### Database configs ###

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=portal
DB_USERNAME=root
DB_PASSWORD=111111

#### Mail configs for Postmark ####

MAIL_DRIVER=smtp
MAIL_HOST=smtp.postmarkapp.com
MAIL_PORT=587
MAIL_USERNAME=02d4fd44-8756-4342-9ca5-5297223b5561
MAIL_PASSWORD=02d4fd44-8756-4342-9ca5-5297223b5561
MAIL_ENCRYPTION=tls
MAIL_MAILER=smtp

VERIFY_LOGIN_TIME=600
FRONTEND_APP_URL=http://localhost:3000

TEST_USER_1_EMAIL=test@blackhole.postmarkapp.com
TEST_USER_2_EMAIL=portaluser1@mailinator.com
TEST_USER_3_EMAIL=portaluser2@mailinator.com
TEST_USER_4_EMAIL=portaluser3@mailinator.com
```

* composer install
* php artisan key:generate
* php artisan config:clear
* php artisan route:clear
* php artisan migrate:refresh --seed
* php artisan serve

```
* Note: http://localhost:8000
* Login email: test@blackhole.postmarkapp.com
* Insead of "php artisan migrate:refresh --seed" command you can use "database/portal.sql"
```

##### About Sending emails via Postmark
The Laravel Postmark Provider is no longer being actively updated.
The following is a short discussion on the reasoning for discontinuing the provider: 
https://github.com/wildbit/laravel-postmark-provider/issues/4#issuecomment-238529465

If it is important to use Postmark as provider we can add in the future. 
So now we are sing only **smtp** of that. Please provide correct and valid emails for **Test Mode**.
  
