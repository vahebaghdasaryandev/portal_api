<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $testUsers = [
            ['name' => env('TEST_USER_1_EMAIL', 'test'), 'email' => env('TEST_USER_1_EMAIL', 'test@blackhole.postmarkapp.com')],
            ['name' => env('TEST_USER_2_EMAIL', 'portaluser1'), 'email' => env('TEST_USER_2_EMAIL', 'portaluser1@mailinator.com')],
            ['name' => env('TEST_USER_3_EMAIL', 'portaluser2'), 'email' => env('TEST_USER_3_EMAIL', 'portaluser2@mailinator.com')],
            ['name' => env('TEST_USER_4_EMAIL', 'portaluser3'), 'email' => env('TEST_USER_4_EMAIL', 'portaluser3@mailinator.com')],
        ];

        foreach ($testUsers as $testUser) {
            $user = new User([
                'name' => $testUser['name'],
                'email' => $testUser['email'],
                'login_uuid' => Str::uuid()->toString(),
                'login_link' => null,
                'auth_token' => Str::random(32)
            ]);
            $user->save();
        }
    }
}
