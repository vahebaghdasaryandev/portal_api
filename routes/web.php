<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('verifyLogin/{loginLink?}', 'AuthController@verifyLogin')->name('verifyLogin');
Route::post('sendLoginLink', 'AuthController@sendLoginLink')->name('sendLoginLink');
Route::get('logout', 'AuthController@logout')->name('logout');

Route::get('me', 'DashboardController@me')->name('dashboard.me');
Route::get('userEmails', 'DashboardController@userEmails')->name('dashboard.userEmails');
Route::post('saveUserEmails', 'DashboardController@saveUserEmails')->name('dashboard.saveUserEmails');

Route::any('{name?}', 'AuthController@accessDenied')->name('accessDenied');
